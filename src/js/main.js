(function () {
    'use strict';
    var _scriptId = 'shareroot',
        _element = '#gallery',
        _galleryId = 281,
        _uploadId = 282;

    var _appendScripts = function () {
        (function (url, conf) {
            var s = document.createElement("script");
            s.src = url;
            s.id = _scriptId;
            s.onload = function () {
                window._shareroot.config(conf);
            };
            (document.getElementsByTagName("head")[0] || document.body).appendChild(s);
        })("//sdk-sharerootinc.netdna-ssl.com/js/sdk.js", {
            // upload: {id: _uploadId, hash: "upload"},
            gallery: {id: _galleryId, hash: "image", element: _element}
        });
        return this;
    };

    var _appendBtn = function () {
        var html = '<a target="_blank" href="//www.autotrader.com/?LNX=SOMEDSHAREROOT">Find My Match</a>';
        var btn = document.getElementById('upload-btn');
        btn.innerHTML = '';
        btn.innerHTML = html;
        return this;
    };


    _appendScripts();
    _appendBtn();
})();
